<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mamboo_tienda' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4#ae)mo=|n{~aSXhP6 a:z3VyHY0h6zX>%7JtRJ.>q#il[b%&*/dc19xzw_>M5.G' );
define( 'SECURE_AUTH_KEY',  'Kxv9G&<a--zTBV(6u28MXSuv!bG(|m%`:v:0$C$)nQCV L2:y&;`b4uiEARHDYo$' );
define( 'LOGGED_IN_KEY',    '0+RE3Ir2s/ToJ,CT$~x%&?9F#hgMQe:~3mo8cGnIKzh<H;D*bt2(K.NbKH~F`w ;' );
define( 'NONCE_KEY',        'feV0KzvgKP)qVEJ+73xhq_# l-7P1Rl1rHIu2)z*v>8Z*,I)4BWSS>)jfSX?ALA/' );
define( 'AUTH_SALT',        'Z*G`|hLL%l>AAO~@.E+2zV<LyC:;rR-AH@v1b2g?/Q4Sl%]2zVoM `vnw2g#|1=1' );
define( 'SECURE_AUTH_SALT', 'd8}OS`QPhTEFy)EpYG=$d1;>,1}MPmx=W40urXp3o3QCgQA3trK+D:]wP?I:hyr<' );
define( 'LOGGED_IN_SALT',   'X%HG:^>#+&x8*j#t&2EQay24~$?|IHn~%D6HBg]GOf5Oj|ubXL5,l5}N,.6ah`J0' );
define( 'NONCE_SALT',       'n:C!(l/T6re)~ YwkNbb-fGA|YLz$_5k>Ip:YrY1 ,lp*OC44zQ$v8nDT4lI+Yrq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
