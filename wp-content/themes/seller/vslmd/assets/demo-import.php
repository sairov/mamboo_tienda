<?php 

/*-----------------------------------------------------------------------------------*/
/*	One Click Demo Import
/*-----------------------------------------------------------------------------------*/

//Change the location, title and other parameters of the plugin page
function ocdi_plugin_page_setup( $default_settings ) {
    $default_settings['parent_slug'] = 'themes.php';
    $default_settings['menu_title']  = esc_html__( 'Visualmodo Demo' , 'vslmd' );
    $default_settings['menu_slug']   = 'visualmodo-demo-import';

    return $default_settings;
}
add_filter( 'pt-ocdi/plugin_page_setup', 'ocdi_plugin_page_setup' );

//Disable the ProteusThemes branding notice with a WP filter
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

$product = wp_get_theme()->get( 'Name' );

if( $product == 'Edge' ) {

function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Edge Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/edge/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/edge/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/edge/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/edge/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                /*array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/edge/visual-elements.json',
                    'option_name' => 've_options',
                ),*/
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/edge/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Fitness' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Fitness Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/fitness/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/fitness/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/fitness/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/fitness/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                /*array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/fitness/visual-elements.json',
                    'option_name' => 've_options',
                ),*/
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/fitness/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Gym' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Gym Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/gym/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/gym/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/gym/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/gym/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/gym/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/gym/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Zenith' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Zenith Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/zenith/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/zenith/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/zenith/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/zenith/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/zenith/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/zenith/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Footer Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer_menu->term_id 
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Sport' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Sport Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/sport/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/sport/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/sport/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/sport/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/sport/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/sport/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $one_page = get_term_by( 'name', 'One Page', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'one_page' => $one_page->term_id 
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Food' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Food Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/food/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/food/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/food/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/food/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/food/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/food/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $top_menu = get_term_by( 'name', 'Top Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'top_menu' => $top_menu->term_id 
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Peak' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Peak Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/peak/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/peak/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/peak/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/peak/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/peak/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/peak/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $side_menu = get_term_by( 'name', 'Side', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'side_menu' => $side_menu->term_id 
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Spark' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Spark Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/spark/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/spark/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/spark/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/spark/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/spark/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/spark/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $side_menu = get_term_by( 'name', 'Side', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'side' => $side_menu->term_id 
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Stream' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Stream Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/stream/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/stream/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/stream/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/stream/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/stream/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/stream/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $side_menu = get_term_by( 'name', 'Side', 'nav_menu' );
    $one_page = get_term_by( 'name', 'One Page', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'side_menu' => $side_menu->term_id,
            'one_page' => $one_page->term_id 
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Ink' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Ink Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/ink/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/ink/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/ink/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/ink/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/ink/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/ink/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $side_menu = get_term_by( 'name', 'Side Menu', 'nav_menu' );
    $categories = get_term_by( 'name', 'Categories', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'side_menu' => $side_menu->term_id,
            'categories' => $categories->term_id 
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Beyond' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Beyond Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/beyond/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/beyond/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/beyond/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/beyond/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/beyond/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/beyond/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Rare' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Rare Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/rare/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/rare/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/rare/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/rare/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/rare/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/rare/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );
    $side_navigation = get_term_by( 'Side Navigation', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer->term_id,
            'side_navigation' => $side_navigation->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Wedding' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Wedding Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/wedding/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/wedding/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/wedding/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/wedding/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                /*array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/wedding/visual-elements.json',
                    'option_name' => 've_options',
                ),*/
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/wedding/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Architect' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Architect Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/architect/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/architect/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/architect/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/architect/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/architect/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/architect/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Medical' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Medical Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/medical/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/medical/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/medical/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/medical/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/medical/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/medical/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Marvel' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Marvel Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/marvel/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/marvel/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/marvel/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/marvel/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/marvel/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/marvel/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $copyright_menu = get_term_by( 'name', 'Copyright Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'copyright_menu' => $copyright_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Seller' ) {

	function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Seller Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/seller/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/seller/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/seller/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/seller/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/seller/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/seller/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Winehouse' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Winehouse Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/winehouse/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/winehouse/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/winehouse/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/winehouse/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/winehouse/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/winehouse/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer->term_id
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Nectar' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Nectar Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/nectar/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/nectar/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/nectar/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/nectar/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/nectar/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/nectar/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main / mobile', 'nav_menu' );
    $left = get_term_by( 'name', 'Left', 'nav_menu' );
    $right = get_term_by( 'name', 'Right', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'left' => $left->term_id,
            'right' => $right->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Mechanic' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Mechanic Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/mechanic/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/mechanic/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/mechanic/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/mechanic/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/mechanic/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/mechanic/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Construction' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Construction Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/construction/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/construction/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/construction/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/construction/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/construction/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/construction/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Traveler' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Traveler Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/traveler/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/traveler/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/traveler/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/traveler/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/traveler/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/traveler/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $categories = get_term_by( 'name', 'Categories', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'categories' => $categories->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Salon' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Salon Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/salon/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/salon/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/salon/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/salon/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/salon/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/salon/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Music' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Music Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/music/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/music/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/music/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/music/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/music/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/music/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Resume' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Resume Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/resume/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/resume/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/resume/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/resume/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/resume/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/resume/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $portfolio_menu = get_term_by( 'name', 'Portfolio Item Page Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'portfolio' => $portfolio_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    //$blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    //update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Hotel' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Hotel Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/hotel/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/hotel/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/hotel/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/hotel/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/hotel/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/hotel/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Cryptocurrency' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Cryptocurrency Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/cryptocurrency/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/cryptocurrency/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/cryptocurrency/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/cryptocurrency/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/cryptocurrency/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/cryptocurrency/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer_menu = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Dark' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Dark Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/dark/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/dark/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/dark/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/dark/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/dark/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/dark/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Nonprofit' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Nonprofit Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/nonprofit/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/nonprofit/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/nonprofit/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/nonprofit/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/nonprofit/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/nonprofit/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Employment' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Employment Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/employment/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/employment/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/employment/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/employment/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/employment/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/employment/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Forum' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Forum Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/forum/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/forum/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/forum/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/forum/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/forum/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/forum/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Petshop' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Petshop Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/petshop/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/petshop/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/petshop/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/petshop/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/petshop/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/petshop/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Photography' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Photography Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/photography/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/photography/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/photography/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/photography/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/photography/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/photography/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $main = get_term_by( 'name', 'Footer Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Education' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Education Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/education/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/education/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/education/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/education/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/education/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/education/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $side = get_term_by( 'name', 'Side Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'side' => $side->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Minimalist' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Minimalist Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/minimalist/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/minimalist/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/minimalist/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/minimalist/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/minimalist/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/minimalist/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main Menu', 'nav_menu' );
    $side = get_term_by( 'name', 'Side Navigation', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );
    $fotter_right = get_term_by( 'name', 'Footer Right', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'side' => $side->term_id,
            'footer' => $footer->term_id,
            'fotter_right' => $fotter_right->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Cafe' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Cafe Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/cafe/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/cafe/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/cafe/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/cafe/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/cafe/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/cafe/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main', 'nav_menu' );
    $top_menu = get_term_by( 'name', 'Top Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'top_menu' => $top_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'IT' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'IT Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/it/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/it/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/it/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/it/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/it/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/it/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Financial' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Financial Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/financial/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/financial/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/financial/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/financial/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/financial/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/financial/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );
    $services = get_term_by( 'name', 'Services', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'footer' => $footer->term_id,
            'services' => $services->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Politic' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Politic Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/politic/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/politic/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/politic/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/politic/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/politic/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/politic/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );
    $proposals = get_term_by( 'name', 'Proposals', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'footer' => $footer->term_id,
            'proposals' => $proposals->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Realestate' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Real Estate Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/realestate/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/realestate/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/realestate/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/realestate/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/realestate/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/realestate/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Church' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Church Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/church/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/church/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/church/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/church/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/church/visual-elements.json',
                    'option_name' => 've_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/church/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main', 'nav_menu' );
    $footer = get_term_by( 'name', 'Footer', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
            'footer' => $footer->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

} elseif( $product == 'Visualmentor' ) {

    function vslmd_import_files() {
    return array(
        array(
            'import_file_name'           => 'Visualmentor Demo',
            //'categories'                 => array( 'Business', 'portfolio' ),
            'import_file_url'            => 'https://cdn.visualmodo.com/demo/visualmentor/demo-content.xml',
            'import_widget_file_url'     => 'https://cdn.visualmodo.com/demo/visualmentor/widgets.wie',
            'import_customizer_file_url' => 'https://cdn.visualmodo.com/demo/visualmentor/customizer.dat',
            'import_redux'               => array(
                array(
                    'file_url'    => 'https://cdn.visualmodo.com/demo/visualmentor/theme-options.json',
                    'option_name' => 'vslmd_options',
                ),
            ),
            'import_preview_image_url'   => 'https://cdn.visualmodo.com/demo/import-demo-cover.png',
            'import_notice'              => __( 'After you import this demo, you will have to setup the slider separately.', 'vslmd' ),
            'preview_url'                => 'https://theme.visualmodo.com/visualmentor/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'vslmd_import_files' );


// Assign Front page, Posts page and menu locations after the importer

function vslmd_after_import_setup() {

    // Assign menus to their locations.
    $main = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'vslmd_after_import_setup' );

}

