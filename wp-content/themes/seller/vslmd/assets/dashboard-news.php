<?php
add_action( 'wp_dashboard_setup', 'vm_dashboard_add_widget_visualmodo_news' );
function vm_dashboard_add_widget_visualmodo_news() {
	wp_add_dashboard_widget( 
		'vm_dashboard_widget_news', 
		__( 'Visualmodo News', 'vslmd' ), 
		'vm_dashboard_widget_news_handler', 
		'vm_dashboard_widget_news_config_handler'
	);
}

function vm_dashboard_widget_news_handler() {
	$options = wp_parse_args( get_option( 'vm_dashboard_widget_news' ), vm_dashboard_widget_news_config_defaults() );

	$feeds = array(
		array(
			'url'          => 'https://www.visualmodo.com/feed/',
			'items'        => $options['items'],
			'show_summary' => 0,
			'show_author'  => 0,
			'show_date'    => 0,
		),
	);

	wp_dashboard_primary_output( 'vm_dashboard_widget_news', $feeds );
}

function vm_dashboard_widget_news_config_defaults() {
	return array(
		'items' => 5,
	);
}

function vm_dashboard_widget_news_config_handler() {
	$options = wp_parse_args( get_option( 'vm_dashboard_widget_news' ), vm_dashboard_widget_news_config_defaults() );

	if ( isset( $_POST['submit'] ) ) {
		if ( isset( $_POST['rss_items'] ) && intval( $_POST['rss_items'] ) > 0 ) {
			$options['items'] = intval( $_POST['rss_items'] );
		}

		update_option( 'vm_dashboard_widget_news', $options );
	}

    ?>
	<p>
		<label><?php _e( 'Number of RSS articles:', 'vslmd' ); ?>
			<input type="text" name="rss_items" value="<?php echo esc_attr( $options['items'] ); ?>" />
		</label>
	</p>
	<?php
}
